[Ivy]
[>Created: Fri Aug 11 15:19:43 ICT 2017]
15DAC104698B8EF4 3.18 #module
>Proto >Proto Collection #zClass
Ss0 SendingLeaveRequestPageProcess Big #zClass
Ss0 RD #cInfo
Ss0 #process
Ss0 @TextInP .ui2RdDataAction .ui2RdDataAction #zField
Ss0 @TextInP .rdData2UIAction .rdData2UIAction #zField
Ss0 @TextInP .resExport .resExport #zField
Ss0 @TextInP .type .type #zField
Ss0 @TextInP .processKind .processKind #zField
Ss0 @AnnotationInP-0n ai ai #zField
Ss0 @MessageFlowInP-0n messageIn messageIn #zField
Ss0 @MessageFlowOutP-0n messageOut messageOut #zField
Ss0 @TextInP .xml .xml #zField
Ss0 @TextInP .responsibility .responsibility #zField
Ss0 @RichDialogInitStart f0 '' #zField
Ss0 @RichDialogProcessEnd f1 '' #zField
Ss0 @RichDialogProcessStart f3 '' #zField
Ss0 @RichDialogEnd f4 '' #zField
Ss0 @Alternative f8 '' #zField
Ss0 @RichDialogProcessEnd f10 '' #zField
Ss0 @PushWFArc f11 '' #zField
Ss0 @GridStep f6 '' #zField
Ss0 @PushWFArc f7 '' #zField
Ss0 @PushWFArc f2 '' #zField
Ss0 @PushWFArc f18 '' #zField
Ss0 @PushWFArc f5 '' #zField
>Proto Ss0 Ss0 SendingLeaveRequestPageProcess #zField
Ss0 f0 guid 15DAC1046A7E4F01 #txt
Ss0 f0 type ch.fintech.page.SendingLeaveRequestPage.SendingLeaveRequestPageData #txt
Ss0 f0 method start() #txt
Ss0 f0 disableUIEvents true #txt
Ss0 f0 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Ss0 f0 outParameterDecl '<ch.fintech.LeaveRequestInformation leaveRequestInformation> result;
' #txt
Ss0 f0 outParameterMapAction 'result.leaveRequestInformation=in.leaveRequestInformation;
' #txt
Ss0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start()</name>
        <nameStyle>7,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ss0 f0 83 51 26 26 -16 15 #rect
Ss0 f0 @|RichDialogInitStartIcon #fIcon
Ss0 f1 type ch.fintech.page.SendingLeaveRequestPage.SendingLeaveRequestPageData #txt
Ss0 f1 435 51 26 26 0 12 #rect
Ss0 f1 @|RichDialogProcessEndIcon #fIcon
Ss0 f3 guid 15DAC1046B3E0EAD #txt
Ss0 f3 type ch.fintech.page.SendingLeaveRequestPage.SendingLeaveRequestPageData #txt
Ss0 f3 actionDecl 'ch.fintech.page.SendingLeaveRequestPage.SendingLeaveRequestPageData out;
' #txt
Ss0 f3 actionTable 'out=in;
' #txt
Ss0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>close</name>
        <nameStyle>5,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ss0 f3 83 147 26 26 -15 15 #rect
Ss0 f3 @|RichDialogProcessStartIcon #fIcon
Ss0 f4 type ch.fintech.page.SendingLeaveRequestPage.SendingLeaveRequestPageData #txt
Ss0 f4 guid 15DAC1046B3E7C56 #txt
Ss0 f4 347 147 26 26 0 12 #rect
Ss0 f4 @|RichDialogEndIcon #fIcon
Ss0 f8 type ch.fintech.page.SendingLeaveRequestPage.SendingLeaveRequestPageData #txt
Ss0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>End Date is equal or greater than Start Date</name>
        <nameStyle>44,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ss0 f8 200 144 32 32 -77 -43 #rect
Ss0 f8 @|AlternativeIcon #fIcon
Ss0 f10 type ch.fintech.page.SendingLeaveRequestPage.SendingLeaveRequestPageData #txt
Ss0 f10 203 219 26 26 0 12 #rect
Ss0 f10 @|RichDialogProcessEndIcon #fIcon
Ss0 f11 expr in #txt
Ss0 f11 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>No</name>
        <nameStyle>2,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ss0 f11 216 176 216 219 #arcP
Ss0 f11 0 0.46511627906976744 -11 0 #arcLabel
Ss0 f6 actionDecl 'ch.fintech.page.SendingLeaveRequestPage.SendingLeaveRequestPageData out;
' #txt
Ss0 f6 actionTable 'out=in;
' #txt
Ss0 f6 actionCode 'in.leaveRequestInformation.fullname = ivy.session.getSessionUser().getFullName();
in.leaveRequestInformation.email = ivy.session.getSessionUser().getEMailAddress();' #txt
Ss0 f6 type ch.fintech.page.SendingLeaveRequestPage.SendingLeaveRequestPageData #txt
Ss0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Fill Full name and Email field</name>
        <nameStyle>30,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ss0 f6 176 42 176 44 -79 -8 #rect
Ss0 f6 @|StepIcon #fIcon
Ss0 f7 expr out #txt
Ss0 f7 109 64 176 64 #arcP
Ss0 f2 expr out #txt
Ss0 f2 352 64 435 64 #arcP
Ss0 f18 expr in #txt
Ss0 f18 outCond 'in.leaveRequestInformation.endDate >= in.leaveRequestInformation.startDate' #txt
Ss0 f18 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Yes</name>
        <nameStyle>3,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ss0 f18 232 160 347 160 #arcP
Ss0 f18 0 0.4749999999999999 0 8 #arcLabel
Ss0 f5 expr out #txt
Ss0 f5 109 160 200 160 #arcP
>Proto Ss0 .type ch.fintech.page.SendingLeaveRequestPage.SendingLeaveRequestPageData #txt
>Proto Ss0 .processKind HTML_DIALOG #txt
>Proto Ss0 -8 -8 16 16 16 26 #rect
>Proto Ss0 '' #fIcon
Ss0 f11 head f10 mainIn #connect
Ss0 f0 mainOut f7 tail #connect
Ss0 f7 head f6 mainIn #connect
Ss0 f6 mainOut f2 tail #connect
Ss0 f2 head f1 mainIn #connect
Ss0 f8 out f18 tail #connect
Ss0 f18 head f4 mainIn #connect
Ss0 f8 out f11 tail #connect
Ss0 f3 mainOut f5 tail #connect
Ss0 f5 head f8 in #connect
