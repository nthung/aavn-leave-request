[Ivy]
[>Created: Thu Aug 10 21:39:33 ICT 2017]
15DC095CA688938D 3.18 #module
>Proto >Proto Collection #zClass
Ms0 ManagedLeaveRequestProcess Big #zClass
Ms0 RD #cInfo
Ms0 #process
Ms0 @TextInP .ui2RdDataAction .ui2RdDataAction #zField
Ms0 @TextInP .rdData2UIAction .rdData2UIAction #zField
Ms0 @TextInP .resExport .resExport #zField
Ms0 @TextInP .type .type #zField
Ms0 @TextInP .processKind .processKind #zField
Ms0 @AnnotationInP-0n ai ai #zField
Ms0 @MessageFlowInP-0n messageIn messageIn #zField
Ms0 @MessageFlowOutP-0n messageOut messageOut #zField
Ms0 @TextInP .xml .xml #zField
Ms0 @TextInP .responsibility .responsibility #zField
Ms0 @RichDialogInitStart f0 '' #zField
Ms0 @GridStep f6 '' #zField
Ms0 @PushWFArc f7 '' #zField
Ms0 @PushWFArc f2 '' #zField
Ms0 @RichDialogProcessEnd f1 '' #zField
Ms0 @RichDialogEnd f10 '' #zField
Ms0 @RichDialogEnd f12 '' #zField
Ms0 @RichDialogProcessStart f8 '' #zField
Ms0 @RichDialogProcessStart f9 '' #zField
Ms0 @EMail f3 '' #zField
Ms0 @PushWFArc f4 '' #zField
Ms0 @PushWFArc f5 '' #zField
Ms0 @EMail f13 '' #zField
Ms0 @PushWFArc f16 '' #zField
Ms0 @PushWFArc f11 '' #zField
>Proto Ms0 Ms0 ManagedLeaveRequestProcess #zField
Ms0 f0 guid 15DC095CAA56DAB2 #txt
Ms0 f0 type ch.fintech.page.ManagedLeaveRequest.ManagedLeaveRequestData #txt
Ms0 f0 method start(ch.fintech.LeaveRequestInformation) #txt
Ms0 f0 disableUIEvents true #txt
Ms0 f0 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<ch.fintech.LeaveRequestInformation leaveRequestInformation> param = methodEvent.getInputArguments();
' #txt
Ms0 f0 inParameterMapAction 'out.leaveRequestInformation=param.leaveRequestInformation;
' #txt
Ms0 f0 outParameterDecl '<> result;
' #txt
Ms0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start(LeaveRequestInformation)</name>
        <nameStyle>30,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ms0 f0 83 51 26 26 -87 15 #rect
Ms0 f0 @|RichDialogInitStartIcon #fIcon
Ms0 f6 actionDecl 'ch.fintech.page.ManagedLeaveRequest.ManagedLeaveRequestData out;
' #txt
Ms0 f6 actionTable 'out=in;
' #txt
Ms0 f6 actionCode 'import ch.ivyteam.ivy.workflow.query.TaskQuery;
import ch.ivyteam.ivy.workflow.ITask;

TaskQuery query = TaskQuery.create().where().currentUserCanWorkOn();
out.tasks = ivy.wf.getTaskQueryExecutor().getResults(query);' #txt
Ms0 f6 security system #txt
Ms0 f6 type ch.fintech.page.ManagedLeaveRequest.ManagedLeaveRequestData #txt
Ms0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Get Ivy tasks</name>
        <nameStyle>13,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ms0 f6 200 42 112 44 -34 -8 #rect
Ms0 f6 @|StepIcon #fIcon
Ms0 f7 expr out #txt
Ms0 f7 109 64 200 64 #arcP
Ms0 f2 expr out #txt
Ms0 f2 312 64 395 64 #arcP
Ms0 f1 type ch.fintech.page.ManagedLeaveRequest.ManagedLeaveRequestData #txt
Ms0 f1 395 51 26 26 0 12 #rect
Ms0 f1 @|RichDialogProcessEndIcon #fIcon
Ms0 f10 type ch.fintech.page.ManagedLeaveRequest.ManagedLeaveRequestData #txt
Ms0 f10 guid 15DC095CACEA4E42 #txt
Ms0 f10 387 156 26 26 0 12 #rect
Ms0 f10 @|RichDialogEndIcon #fIcon
Ms0 f12 type ch.fintech.page.ManagedLeaveRequest.ManagedLeaveRequestData #txt
Ms0 f12 guid 15DC5141460DD19B #txt
Ms0 f12 395 251 26 26 0 12 #rect
Ms0 f12 @|RichDialogEndIcon #fIcon
Ms0 f8 guid 15DCA7ABDC8BD63D #txt
Ms0 f8 type ch.fintech.page.ManagedLeaveRequest.ManagedLeaveRequestData #txt
Ms0 f8 actionDecl 'ch.fintech.page.ManagedLeaveRequest.ManagedLeaveRequestData out;
' #txt
Ms0 f8 actionTable 'out=in;
' #txt
Ms0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>reject</name>
        <nameStyle>6,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ms0 f8 79 252 26 26 -15 15 #rect
Ms0 f8 @|RichDialogProcessStartIcon #fIcon
Ms0 f9 guid 15DCA7ABDC921E3E #txt
Ms0 f9 type ch.fintech.page.ManagedLeaveRequest.ManagedLeaveRequestData #txt
Ms0 f9 actionDecl 'ch.fintech.page.ManagedLeaveRequest.ManagedLeaveRequestData out;
' #txt
Ms0 f9 actionTable 'out=in;
' #txt
Ms0 f9 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>approve</name>
        <nameStyle>7,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ms0 f9 79 156 26 26 -22 15 #rect
Ms0 f9 @|RichDialogProcessStartIcon #fIcon
Ms0 f3 beanConfig '"{/emailSubject ""Inform for a leave request""/emailFrom ""leave.request.no.reply@axonactive.com""/emailReplyTo """"/emailTo ""<%=in.leaveRequestInformation.email%>""/emailCC """"/emailBCC """"/exceptionMissingEmailAttachments ""false""/emailMessage ""Dear <%=in.leaveRequestInformation.fullname%>,\\n\\nYour leave request from <%=in.leaveRequestInformation.startDate%> to <%=in.leaveRequestInformation.endDate%> with <%=in.leaveRequestInformation.type%> reason has been Approved by Superior.\\n\\nThanks,\\nSuperior.""/emailAttachments * }"' #txt
Ms0 f3 type ch.fintech.page.ManagedLeaveRequest.ManagedLeaveRequestData #txt
Ms0 f3 timeout 0 #txt
Ms0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Send email for confirmation</name>
        <nameStyle>27,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ms0 f3 160 147 160 44 -75 -8 #rect
Ms0 f3 @|EMailIcon #fIcon
Ms0 f4 expr out #txt
Ms0 f4 105 169 160 169 #arcP
Ms0 f5 expr out #txt
Ms0 f5 320 169 387 169 #arcP
Ms0 f13 beanConfig '"{/emailSubject ""Inform for a leave request""/emailFrom ""leave.request.no.reply@axonactive.com""/emailReplyTo """"/emailTo ""<%=in.leaveRequestInformation.email%>""/emailCC """"/emailBCC """"/exceptionMissingEmailAttachments ""false""/emailMessage ""Dear <%=in.leaveRequestInformation.fullname%>,\\n\\nYour leave request from <%=in.leaveRequestInformation.startDate%> to <%=in.leaveRequestInformation.endDate%> with <%=in.leaveRequestInformation.type%> reason has been Rejected by Superior.\\n\\nThanks,\\nSuperior.""/emailAttachments * }"' #txt
Ms0 f13 type ch.fintech.page.ManagedLeaveRequest.ManagedLeaveRequestData #txt
Ms0 f13 timeout 0 #txt
Ms0 f13 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Send email for confirmation</name>
        <nameStyle>27,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ms0 f13 160 242 160 44 -75 -8 #rect
Ms0 f13 @|EMailIcon #fIcon
Ms0 f16 expr out #txt
Ms0 f16 104 264 160 264 #arcP
Ms0 f11 expr out #txt
Ms0 f11 320 264 395 264 #arcP
>Proto Ms0 .type ch.fintech.page.ManagedLeaveRequest.ManagedLeaveRequestData #txt
>Proto Ms0 .processKind HTML_DIALOG #txt
>Proto Ms0 -8 -8 16 16 16 26 #rect
>Proto Ms0 '' #fIcon
Ms0 f0 mainOut f7 tail #connect
Ms0 f7 head f6 mainIn #connect
Ms0 f6 mainOut f2 tail #connect
Ms0 f2 head f1 mainIn #connect
Ms0 f9 mainOut f4 tail #connect
Ms0 f4 head f3 mainIn #connect
Ms0 f3 mainOut f5 tail #connect
Ms0 f5 head f10 mainIn #connect
Ms0 f8 mainOut f16 tail #connect
Ms0 f16 head f13 mainIn #connect
Ms0 f13 mainOut f11 tail #connect
Ms0 f11 head f12 mainIn #connect
