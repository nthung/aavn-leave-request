[Ivy]
[>Created: Thu Aug 10 17:33:48 ICT 2017]
15DC61DA4733CBC9 3.18 #module
>Proto >Proto Collection #zClass
Hs0 HeaderComponentProcess Big #zClass
Hs0 RD #cInfo
Hs0 #process
Hs0 @TextInP .ui2RdDataAction .ui2RdDataAction #zField
Hs0 @TextInP .rdData2UIAction .rdData2UIAction #zField
Hs0 @TextInP .resExport .resExport #zField
Hs0 @TextInP .type .type #zField
Hs0 @TextInP .processKind .processKind #zField
Hs0 @AnnotationInP-0n ai ai #zField
Hs0 @MessageFlowInP-0n messageIn messageIn #zField
Hs0 @MessageFlowOutP-0n messageOut messageOut #zField
Hs0 @TextInP .xml .xml #zField
Hs0 @TextInP .responsibility .responsibility #zField
Hs0 @RichDialogInitStart f0 '' #zField
Hs0 @RichDialogProcessEnd f1 '' #zField
Hs0 @RichDialogProcessStart f3 '' #zField
Hs0 @RichDialogEnd f4 '' #zField
Hs0 @GridStep f8 '' #zField
Hs0 @PushWFArc f9 '' #zField
Hs0 @PushWFArc f2 '' #zField
Hs0 @GridStep f6 '' #zField
Hs0 @GridStep f7 '' #zField
Hs0 @PushWFArc f11 '' #zField
Hs0 @PushWFArc f5 '' #zField
Hs0 @PushWFArc f10 '' #zField
>Proto Hs0 Hs0 HeaderComponentProcess #zField
Hs0 f0 guid 15DC61DA48DD60D6 #txt
Hs0 f0 type ch.fintech.component.HeaderComponent.HeaderComponentData #txt
Hs0 f0 method start() #txt
Hs0 f0 disableUIEvents true #txt
Hs0 f0 inParameterDecl 'ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent methodEvent = event as ch.ivyteam.ivy.richdialog.exec.RdMethodCallEvent;
<> param = methodEvent.getInputArguments();
' #txt
Hs0 f0 outParameterDecl '<> result;
' #txt
Hs0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start()</name>
        <nameStyle>7,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Hs0 f0 83 51 26 26 -16 15 #rect
Hs0 f0 @|RichDialogInitStartIcon #fIcon
Hs0 f1 type ch.fintech.component.HeaderComponent.HeaderComponentData #txt
Hs0 f1 339 51 26 26 0 12 #rect
Hs0 f1 @|RichDialogProcessEndIcon #fIcon
Hs0 f3 guid 15DC61DA49B84DCD #txt
Hs0 f3 type ch.fintech.component.HeaderComponent.HeaderComponentData #txt
Hs0 f3 actionDecl 'ch.fintech.component.HeaderComponent.HeaderComponentData out;
' #txt
Hs0 f3 actionTable 'out=in;
' #txt
Hs0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>logout</name>
        <nameStyle>6,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Hs0 f3 83 147 26 26 -17 15 #rect
Hs0 f3 @|RichDialogProcessStartIcon #fIcon
Hs0 f4 type ch.fintech.component.HeaderComponent.HeaderComponentData #txt
Hs0 f4 guid 15DC61DA49B3F3A5 #txt
Hs0 f4 499 147 26 26 0 12 #rect
Hs0 f4 @|RichDialogEndIcon #fIcon
Hs0 f8 actionDecl 'ch.fintech.component.HeaderComponent.HeaderComponentData out;
' #txt
Hs0 f8 actionTable 'out=in;
' #txt
Hs0 f8 actionCode 'in.fullname = ivy.session.getSessionUser().getFullName();' #txt
Hs0 f8 type ch.fintech.component.HeaderComponent.HeaderComponentData #txt
Hs0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Get Full name</name>
        <nameStyle>13,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Hs0 f8 168 42 112 44 -38 -8 #rect
Hs0 f8 @|StepIcon #fIcon
Hs0 f9 expr out #txt
Hs0 f9 109 64 168 64 #arcP
Hs0 f2 expr out #txt
Hs0 f2 280 64 339 64 #arcP
Hs0 f6 actionDecl 'ch.fintech.component.HeaderComponent.HeaderComponentData out;
' #txt
Hs0 f6 actionTable 'out=in;
' #txt
Hs0 f6 actionCode ivy.session.logoutSessionUser(); #txt
Hs0 f6 type ch.fintech.component.HeaderComponent.HeaderComponentData #txt
Hs0 f6 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Clear session</name>
        <nameStyle>13,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Hs0 f6 168 138 112 44 -39 -8 #rect
Hs0 f6 @|StepIcon #fIcon
Hs0 f7 actionDecl 'ch.fintech.component.HeaderComponent.HeaderComponentData out;
' #txt
Hs0 f7 actionTable 'out=in;
' #txt
Hs0 f7 actionCode 'import javax.faces.context.FacesContext;

FacesContext.getCurrentInstance().getExternalContext().redirect(ivy.html.startref("Start Processes/Login/start.ivp"));' #txt
Hs0 f7 type ch.fintech.component.HeaderComponent.HeaderComponentData #txt
Hs0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Redirect to Login page</name>
        <nameStyle>22,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Hs0 f7 320 138 128 44 -61 -8 #rect
Hs0 f7 @|StepIcon #fIcon
Hs0 f11 expr out #txt
Hs0 f11 280 160 320 160 #arcP
Hs0 f5 expr out #txt
Hs0 f5 448 160 499 160 #arcP
Hs0 f10 expr out #txt
Hs0 f10 109 160 168 160 #arcP
Hs0 f10 0 0.4584867830993195 0 0 #arcLabel
>Proto Hs0 .type ch.fintech.component.HeaderComponent.HeaderComponentData #txt
>Proto Hs0 .processKind HTML_DIALOG #txt
>Proto Hs0 -8 -8 16 16 16 26 #rect
>Proto Hs0 '' #fIcon
Hs0 f0 mainOut f9 tail #connect
Hs0 f9 head f8 mainIn #connect
Hs0 f8 mainOut f2 tail #connect
Hs0 f2 head f1 mainIn #connect
Hs0 f6 mainOut f11 tail #connect
Hs0 f11 head f7 mainIn #connect
Hs0 f7 mainOut f5 tail #connect
Hs0 f5 head f4 mainIn #connect
Hs0 f3 mainOut f10 tail #connect
Hs0 f10 head f6 mainIn #connect
