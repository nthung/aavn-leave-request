package ch.fintech.enums;

public enum LeaveType {
	WORKING_HOLIDAY("Working holiday"),
	MARRIAGE("Marriage"),
	SICKNESS("Sickness"),
	MATERNITY_LEAVE("Maternity"),
	UNPAID("Unpaid"),
	OTHER("Other");

	private String displayName;

	private LeaveType(String displayName) {
		this.displayName = displayName;
	}

	public String getDisplayName() {
		return displayName;
	}
}
