[Ivy]
[>Created: Wed Aug 09 10:37:17 ICT 2017]
15DC0F077FAEEB3C 3.18 #module
>Proto >Proto Collection #zClass
Mt0 ManagedLeaveRequest Big #zClass
Mt0 B #cInfo
Mt0 #process
Mt0 @TextInP .resExport .resExport #zField
Mt0 @TextInP .type .type #zField
Mt0 @TextInP .processKind .processKind #zField
Mt0 @AnnotationInP-0n ai ai #zField
Mt0 @MessageFlowInP-0n messageIn messageIn #zField
Mt0 @MessageFlowOutP-0n messageOut messageOut #zField
Mt0 @TextInP .xml .xml #zField
Mt0 @TextInP .responsibility .responsibility #zField
Mt0 @StartRequest f0 '' #zField
Mt0 @RichDialog f10 '' #zField
Mt0 @EndTask f14 '' #zField
Mt0 @PushWFArc f15 '' #zField
Mt0 @PushWFArc f1 '' #zField
>Proto Mt0 Mt0 ManagedLeaveRequest #zField
Mt0 f0 outLink start.ivp #txt
Mt0 f0 type ch.fintech.LeaveRequestInformation #txt
Mt0 f0 inParamDecl '<> param;' #txt
Mt0 f0 actionDecl 'ch.fintech.LeaveRequestInformation out;
' #txt
Mt0 f0 guid 15DC0F077FFC3B1B #txt
Mt0 f0 requestEnabled true #txt
Mt0 f0 triggerEnabled false #txt
Mt0 f0 callSignature start() #txt
Mt0 f0 persist false #txt
Mt0 f0 startName 'Managed a leave request' #txt
Mt0 f0 startDescription 'Manage a leave request of Employee' #txt
Mt0 f0 taskData 'TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody' #txt
Mt0 f0 showInStartList 1 #txt
Mt0 f0 taskAndCaseSetupAction 'import ch.ivyteam.ivy.workflow.TaskUpdateDefinition;
ch.ivyteam.ivy.workflow.TaskUpdateDefinition taskUpdDef = new ch.ivyteam.ivy.workflow.TaskUpdateDefinition();
import ch.ivyteam.ivy.request.impl.DefaultCalendarProxy;
DefaultCalendarProxy calendarProxy = ivy.cal as DefaultCalendarProxy;
taskUpdDef.setPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
taskUpdDef.setExpiryActivator("Everybody");
taskUpdDef.setExpiryPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
engine.updateCurrentTask(taskUpdDef);
' #txt
Mt0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
        <nameStyle>9,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Mt0 f0 @C|.responsibility Superior #txt
Mt0 f0 81 49 30 30 -21 17 #rect
Mt0 f0 @|StartRequestIcon #fIcon
Mt0 f10 targetWindow NEW:card: #txt
Mt0 f10 targetDisplay TOP #txt
Mt0 f10 richDialogId ch.fintech.page.ManagedLeaveRequest #txt
Mt0 f10 startMethod start(ch.fintech.LeaveRequestInformation) #txt
Mt0 f10 type ch.fintech.LeaveRequestInformation #txt
Mt0 f10 requestActionDecl '<ch.fintech.LeaveRequestInformation leaveRequestInformation> param;' #txt
Mt0 f10 responseActionDecl 'ch.fintech.LeaveRequestInformation out;
' #txt
Mt0 f10 responseMappingAction 'out=in;
' #txt
Mt0 f10 windowConfiguration '* ' #txt
Mt0 f10 isAsynch false #txt
Mt0 f10 isInnerRd false #txt
Mt0 f10 userContext '* ' #txt
Mt0 f10 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show form to approve or reject a leave request</name>
        <nameStyle>46,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Mt0 f10 232 42 272 44 -126 -8 #rect
Mt0 f10 @|RichDialogIcon #fIcon
Mt0 f14 type ch.fintech.LeaveRequestInformation #txt
Mt0 f14 617 49 30 30 0 15 #rect
Mt0 f14 @|EndIcon #fIcon
Mt0 f15 expr out #txt
Mt0 f15 504 64 617 64 #arcP
Mt0 f1 expr out #txt
Mt0 f1 111 64 232 64 #arcP
>Proto Mt0 .type ch.fintech.LeaveRequestInformation #txt
>Proto Mt0 .processKind NORMAL #txt
>Proto Mt0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto Mt0 0 0 32 24 18 0 #rect
>Proto Mt0 @|BIcon #fIcon
Mt0 f10 mainOut f15 tail #connect
Mt0 f15 head f14 mainIn #connect
Mt0 f0 mainOut f1 tail #connect
Mt0 f1 head f10 mainIn #connect
