[Ivy]
[>Created: Wed Aug 09 15:28:43 ICT 2017]
15DAACAA9A6AC9FF 3.18 #module
>Proto >Proto Collection #zClass
Ln0 Login Big #zClass
Ln0 B #cInfo
Ln0 #process
Ln0 @TextInP .resExport .resExport #zField
Ln0 @TextInP .type .type #zField
Ln0 @TextInP .processKind .processKind #zField
Ln0 @AnnotationInP-0n ai ai #zField
Ln0 @MessageFlowInP-0n messageIn messageIn #zField
Ln0 @MessageFlowOutP-0n messageOut messageOut #zField
Ln0 @TextInP .xml .xml #zField
Ln0 @TextInP .responsibility .responsibility #zField
Ln0 @StartRequest f0 '' #zField
Ln0 @RichDialog f3 '' #zField
Ln0 @PushWFArc f4 '' #zField
Ln0 @Alternative f5 '' #zField
Ln0 @PushWFArc f6 '' #zField
Ln0 @EndTask f2 '' #zField
Ln0 @PushWFArc f18 '' #zField
Ln0 @PushWFArc f7 '' #zField
Ln0 @InfoButton f1 '' #zField
Ln0 @AnnotationArc f8 '' #zField
>Proto Ln0 Ln0 Login #zField
Ln0 f0 outLink start.ivp #txt
Ln0 f0 type ch.fintech.Account #txt
Ln0 f0 inParamDecl '<> param;' #txt
Ln0 f0 actionDecl 'ch.fintech.Account out;
' #txt
Ln0 f0 guid 15DAACAAA41B35DD #txt
Ln0 f0 requestEnabled true #txt
Ln0 f0 triggerEnabled false #txt
Ln0 f0 callSignature start() #txt
Ln0 f0 persist false #txt
Ln0 f0 startName 'Login into iLeaveRequest' #txt
Ln0 f0 startDescription 'Login into iLeaveRequest' #txt
Ln0 f0 taskData 'TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody' #txt
Ln0 f0 showInStartList 1 #txt
Ln0 f0 taskAndCaseSetupAction 'import ch.ivyteam.ivy.workflow.TaskUpdateDefinition;
ch.ivyteam.ivy.workflow.TaskUpdateDefinition taskUpdDef = new ch.ivyteam.ivy.workflow.TaskUpdateDefinition();
import ch.ivyteam.ivy.request.impl.DefaultCalendarProxy;
DefaultCalendarProxy calendarProxy = ivy.cal as DefaultCalendarProxy;
taskUpdDef.setPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
taskUpdDef.setExpiryActivator("Everybody");
taskUpdDef.setExpiryPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
engine.updateCurrentTask(taskUpdDef);
' #txt
Ln0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
        <nameStyle>9,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ln0 f0 @C|.responsibility Everybody #txt
Ln0 f0 81 49 30 30 -21 17 #rect
Ln0 f0 @|StartRequestIcon #fIcon
Ln0 f3 targetWindow NEW:card: #txt
Ln0 f3 targetDisplay TOP #txt
Ln0 f3 richDialogId ch.fintech.page.LoginPage #txt
Ln0 f3 startMethod start() #txt
Ln0 f3 type ch.fintech.Account #txt
Ln0 f3 requestActionDecl '<> param;' #txt
Ln0 f3 responseActionDecl 'ch.fintech.Account out;
' #txt
Ln0 f3 responseMappingAction 'out=result.data;
' #txt
Ln0 f3 windowConfiguration '* ' #txt
Ln0 f3 isAsynch false #txt
Ln0 f3 isInnerRd false #txt
Ln0 f3 userContext '* ' #txt
Ln0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show login page</name>
        <nameStyle>15,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ln0 f3 192 42 112 44 -46 -8 #rect
Ln0 f3 @|RichDialogIcon #fIcon
Ln0 f4 expr out #txt
Ln0 f4 111 64 192 64 #arcP
Ln0 f5 type ch.fintech.Account #txt
Ln0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Is sucess?</name>
        <nameStyle>10,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ln0 f5 392 48 32 32 -32 -37 #rect
Ln0 f5 @|AlternativeIcon #fIcon
Ln0 f6 expr out #txt
Ln0 f6 304 64 392 64 #arcP
Ln0 f2 type ch.fintech.Account #txt
Ln0 f2 489 49 30 30 0 15 #rect
Ln0 f2 @|EndIcon #fIcon
Ln0 f18 expr in #txt
Ln0 f18 outCond 'ivy.session.loginSessionUser(in.username, in.password)' #txt
Ln0 f18 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Yes</name>
        <nameStyle>3,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ln0 f18 424 64 489 64 #arcP
Ln0 f18 0 0.5046640156723669 1 -11 #arcLabel
Ln0 f7 expr in #txt
Ln0 f7 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>No</name>
        <nameStyle>2,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ln0 f7 408 80 248 86 #arcP
Ln0 f7 1 408 128 #addKink
Ln0 f7 2 248 128 #addKink
Ln0 f7 0 0.1527777777777778 13 0 #arcLabel
Ln0 f1 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Out of form has a debug</name>
        <nameStyle>23,7
</nameStyle>
    </language>
</elementInfo>
' #txt
Ln0 f1 80 185 144 30 -66 -8 #rect
Ln0 f1 @|IBIcon #fIcon
Ln0 f8 152 185 248 86 #arcP
>Proto Ln0 .type ch.fintech.Account #txt
>Proto Ln0 .processKind NORMAL #txt
>Proto Ln0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto Ln0 0 0 32 24 18 0 #rect
>Proto Ln0 @|BIcon #fIcon
Ln0 f0 mainOut f4 tail #connect
Ln0 f4 head f3 mainIn #connect
Ln0 f3 mainOut f6 tail #connect
Ln0 f6 head f5 in #connect
Ln0 f7 head f3 mainIn #connect
Ln0 f5 out f18 tail #connect
Ln0 f18 head f2 mainIn #connect
Ln0 f5 out f7 tail #connect
Ln0 f1 ao f8 tail #connect
Ln0 f8 head f3 @CG|ai #connect