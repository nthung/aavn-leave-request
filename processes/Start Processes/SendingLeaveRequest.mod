[Ivy]
[>Created: Tue Aug 15 14:36:41 ICT 2017]
15DBB70411C8E6F5 3.18 #module
>Proto >Proto Collection #zClass
St0 SendingLeaveRequest Big #zClass
St0 B #cInfo
St0 #process
St0 @TextInP .resExport .resExport #zField
St0 @TextInP .type .type #zField
St0 @TextInP .processKind .processKind #zField
St0 @AnnotationInP-0n ai ai #zField
St0 @MessageFlowInP-0n messageIn messageIn #zField
St0 @MessageFlowOutP-0n messageOut messageOut #zField
St0 @TextInP .xml .xml #zField
St0 @TextInP .responsibility .responsibility #zField
St0 @StartRequest f0 '' #zField
St0 @EndTask f1 '' #zField
St0 @RichDialog f3 '' #zField
St0 @PushWFArc f4 '' #zField
St0 @UserTask f5 '' #zField
St0 @SignalStartEvent f7 '' #zField
St0 @GridStep f8 '' #zField
St0 @PushWFArc f9 '' #zField
St0 @TkArc f10 '' #zField
St0 @EndTask f11 '' #zField
St0 @PushWFArc f12 '' #zField
St0 @PushWFArc f2 '' #zField
>Proto St0 St0 SendingLeaveRequest #zField
St0 f0 outLink start.ivp #txt
St0 f0 type ch.fintech.LeaveRequestInformation #txt
St0 f0 inParamDecl '<> param;' #txt
St0 f0 actionDecl 'ch.fintech.LeaveRequestInformation out;
' #txt
St0 f0 guid 15DBB70411E7237E #txt
St0 f0 requestEnabled true #txt
St0 f0 triggerEnabled false #txt
St0 f0 callSignature start() #txt
St0 f0 persist false #txt
St0 f0 startName 'Sending a leave request' #txt
St0 f0 startDescription 'Sending a leave request to Superior' #txt
St0 f0 taskData 'TaskTriggered.ROL=Everybody
TaskTriggered.EXTYPE=0
TaskTriggered.EXPRI=2
TaskTriggered.TYPE=0
TaskTriggered.PRI=2
TaskTriggered.EXROL=Everybody' #txt
St0 f0 showInStartList 1 #txt
St0 f0 taskAndCaseSetupAction 'import ch.ivyteam.ivy.workflow.TaskUpdateDefinition;
ch.ivyteam.ivy.workflow.TaskUpdateDefinition taskUpdDef = new ch.ivyteam.ivy.workflow.TaskUpdateDefinition();
import ch.ivyteam.ivy.request.impl.DefaultCalendarProxy;
DefaultCalendarProxy calendarProxy = ivy.cal as DefaultCalendarProxy;
taskUpdDef.setPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
taskUpdDef.setExpiryActivator("Everybody");
taskUpdDef.setExpiryPriority(ch.ivyteam.ivy.workflow.WorkflowPriority.valueOf(2));
engine.updateCurrentTask(taskUpdDef);
' #txt
St0 f0 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>start.ivp</name>
        <nameStyle>9,5,7
</nameStyle>
    </language>
</elementInfo>
' #txt
St0 f0 @C|.responsibility Employee #txt
St0 f0 81 49 30 30 -21 17 #rect
St0 f0 @|StartRequestIcon #fIcon
St0 f1 type ch.fintech.LeaveRequestInformation #txt
St0 f1 609 241 30 30 0 15 #rect
St0 f1 @|EndIcon #fIcon
St0 f3 targetWindow NEW:card: #txt
St0 f3 targetDisplay TOP #txt
St0 f3 richDialogId ch.fintech.page.SendingLeaveRequestPage #txt
St0 f3 startMethod start() #txt
St0 f3 type ch.fintech.LeaveRequestInformation #txt
St0 f3 requestActionDecl '<> param;' #txt
St0 f3 responseActionDecl 'ch.fintech.LeaveRequestInformation out;
' #txt
St0 f3 responseMappingAction 'out=result.leaveRequestInformation;
' #txt
St0 f3 windowConfiguration '* ' #txt
St0 f3 isAsynch false #txt
St0 f3 isInnerRd false #txt
St0 f3 userContext '* ' #txt
St0 f3 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Show sending leave request</name>
        <nameStyle>26,7
</nameStyle>
    </language>
</elementInfo>
' #txt
St0 f3 208 42 160 44 -77 -8 #rect
St0 f3 @|RichDialogIcon #fIcon
St0 f4 expr out #txt
St0 f4 111 64 208 64 #arcP
St0 f5 richDialogId ch.fintech.page.ManagedLeaveRequest #txt
St0 f5 startMethod start(ch.fintech.LeaveRequestInformation) #txt
St0 f5 requestActionDecl '<ch.fintech.LeaveRequestInformation leaveRequestInformation> param;' #txt
St0 f5 requestMappingAction 'param.leaveRequestInformation=in;
' #txt
St0 f5 responseActionDecl 'ch.fintech.LeaveRequestInformation out;
' #txt
St0 f5 responseMappingAction 'out=in;
' #txt
St0 f5 outLinks "TaskA.ivp" #txt
St0 f5 taskData 'TaskA.EXPRI=2
TaskA.EXROL=Superior
TaskA.EXTYPE=0
TaskA.NAM=A leave request for <%\=in.fullname%>
TaskA.PRI=2
TaskA.ROL=Superior
TaskA.SKIP_TASK_LIST=false
TaskA.TYPE=0' #txt
St0 f5 type ch.fintech.LeaveRequestInformation #txt
St0 f5 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Create a leave request and wait for responding</name>
        <nameStyle>46,7
</nameStyle>
    </language>
</elementInfo>
' #txt
St0 f5 208 234 272 44 -128 -8 #rect
St0 f5 @|UserTaskIcon #fIcon
St0 f7 actionDecl 'ch.fintech.LeaveRequestInformation out;
' #txt
St0 f7 actionCode 'import ch.fintech.LeaveRequestInformation;
import com.google.gson.Gson;

Gson gson = new Gson();
out = gson.fromJson(signal.getSignalData() as String, LeaveRequestInformation.class) as LeaveRequestInformation;


' #txt
St0 f7 type ch.fintech.LeaveRequestInformation #txt
St0 f7 signalCode createLeaveRequestTask #txt
St0 f7 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Create leave request task</name>
        <nameStyle>25,7
</nameStyle>
    </language>
</elementInfo>
' #txt
St0 f7 81 241 30 30 -70 17 #rect
St0 f7 @|SignalStartEventIcon #fIcon
St0 f8 actionDecl 'ch.fintech.LeaveRequestInformation out;
' #txt
St0 f8 actionTable 'out=in;
' #txt
St0 f8 actionCode 'import ch.ivyteam.ivy.process.model.value.SignalCode;
import com.google.gson.Gson;

String data = new Gson().toJson(in);
ivy.wf.signals().send(new SignalCode("createLeaveRequestTask"), data);
' #txt
St0 f8 type ch.fintech.LeaveRequestInformation #txt
St0 f8 @C|.xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language>
        <name>Send signal to create a Ivy task</name>
        <nameStyle>32,7
</nameStyle>
    </language>
</elementInfo>
' #txt
St0 f8 456 42 176 44 -84 -8 #rect
St0 f8 @|StepIcon #fIcon
St0 f9 expr out #txt
St0 f9 368 64 456 64 #arcP
St0 f10 type ch.fintech.LeaveRequestInformation #txt
St0 f10 var in2 #txt
St0 f10 111 256 208 256 #arcP
St0 f11 type ch.fintech.LeaveRequestInformation #txt
St0 f11 729 49 30 30 0 15 #rect
St0 f11 @|EndIcon #fIcon
St0 f12 expr out #txt
St0 f12 632 64 729 64 #arcP
St0 f2 expr data #txt
St0 f2 outCond ivp=="TaskA.ivp" #txt
St0 f2 480 256 609 256 #arcP
>Proto St0 .type ch.fintech.LeaveRequestInformation #txt
>Proto St0 .processKind NORMAL #txt
>Proto St0 .xml '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<elementInfo>
    <language/>
</elementInfo>
' #txt
>Proto St0 0 0 32 24 18 0 #rect
>Proto St0 @|BIcon #fIcon
St0 f0 mainOut f4 tail #connect
St0 f4 head f3 mainIn #connect
St0 f3 mainOut f9 tail #connect
St0 f9 head f8 mainIn #connect
St0 f7 mainOut f10 tail #connect
St0 f10 head f5 in #connect
St0 f8 mainOut f12 tail #connect
St0 f12 head f11 mainIn #connect
St0 f5 out f2 tail #connect
St0 f2 head f1 mainIn #connect
